package data;

public class HeroPower {
    private String name;
    private String description;
    private int manaCost;
    private boolean needTarget;

    public HeroPower(String name, String description, int manaCost, boolean needTarget) {
        this.name = name;
        this.description = description;
        this.manaCost = manaCost;
        this.needTarget = needTarget;
    }

    public boolean doesNeedTarget() {
        return needTarget;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getManaCost() {
        return manaCost;
    }
}
