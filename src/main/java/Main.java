import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import data.Hero;
import data.HeroPower;
import database.MySqlMain;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public class Main {

    public static void main(String[] args) {
        MySqlMain database = new MySqlMain();
        try {
            addHeroPowers(database, Hero.getAllHeroes());
            addHeroes(database, Hero.getAllHeroes());
            addCardsToDatabase(database, new File("src/main/resources/cards"));
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
    }

    private static void addHeroes(MySqlMain database, ArrayList<Hero> heroes) throws SQLException {
        final String createTableSQL = "create table heroes(\r\n" + " id int(3) primary key, \r\n" +
                " name varchar(20),\r\n" +
                " specialPower TEXT,\r\n" +
                " heroPower int(3),\r\n" +
                " foreign key (heroPower) references heroPowers(id)\r\n );";
        final String insertDataSQL = "INSERT INTO heroes" +
                " (id, name, specialPower, heroPower) VALUES " +
                " (?,?,?,?);";
        final String heroPowerID = "select id from heroPowers where name=\"%s\"";
        final String insertDataForeignSQL = "INSERT INTO heroPowers (heroPower) VALUES (select id from heroPowers where name='?');";

        database.getWriter().createTable(createTableSQL);

        int id = 1;
        for (Hero hero : heroes) {
            String name = hero.getName();
            String specialPower = hero.getSpecialPower();
            if (specialPower == null) {
                specialPower = " ";
            }
            String heroPower = " ";
            if (hero.getHeroPower() != null) {
                heroPower = hero.getHeroPower().getName();
            }
            database.getWriter().insertData(insertDataSQL, Arrays.asList(id, name, specialPower,
                    database.getReader().getData(String.format(heroPowerID, heroPower))));
            id++;
        }
    }

    private static void addHeroPowers(MySqlMain database, ArrayList<Hero> heroes) throws SQLException {
        final String createTableSQL = "create table heroPowers(\r\n" + " id int(3) primary key, \r\n" +
                " name varchar(20),\r\n" +
                " manaCost int(3),\r\n" +
                " description TEXT,\r\n" +
                " needTarget bool\r\n );";
        final String insertDataSQL = "INSERT INTO heroPowers" +
                " (id, name, manaCost, description, needTarget) VALUES " +
                " (?,?,?,?,?);";

        database.getWriter().createTable(createTableSQL);

        int index = 1;
        for (Hero hero : heroes) {
            HeroPower heroPower = hero.getHeroPower();
//            if (heroPower != null) {
            database.getWriter().insertData(insertDataSQL, Arrays.asList(index,
                    heroPower.getName(), heroPower.getManaCost(), heroPower.getDescription(), heroPower.doesNeedTarget()));
//            }
            index++;
        }
    }


    private static void addCardsToDatabase(MySqlMain database, File file) throws IOException, SQLException {
        final String createTableSQL = "create table cards(\r\n" + " id int(3) primary key, \r\n" +
                " name varchar(20),\r\n" +
                " manaCost int(3),\r\n" +
                " type varchar(20),\r\n" +
                " hero int(3),\r\n" +
                " rarity varchar(20),\r\n" +
                " attack int(3),\r\n" +
                " health int(3),\r\n" +
                " description TEXT,\r\n" +
                " abilityName varchar(20),\r\n" +
                " abilityValue int(3),\r\n " +
                " heroDefaultId int(3) default 0,\r\n" +
                " foreign key (heroDefaultId) references heroes(id),\r\n" +
                " foreign key (hero) references heroes(id)\r\n );";
        final String insertDataSQL = "INSERT INTO cards" +
                " (id, name, manaCost, type, hero, rarity, attack, health, description, abilityName, abilityValue, heroDefaultId) VALUES " +
                " (?,?,?,?,?,?,?,?,?,?,?,?);";
        final String defaultHeroIdSQL = "select id from heroes where name=\"%s\"";

        final String selectDataSQL = "select id,name,manaCost,type,hero,rarity,attack,health,description,abilityName,abilityValue from users where id = ?";
        final String deleteDataSQL = "delete from cards where id = ?;";

        database.getWriter().createTable(createTableSQL);


        Gson gson = new Gson();
        int index = 1;
        for (File heroFolder : new File(file.getPath()).listFiles(File::isDirectory)) {
            for (File cardFolder : heroFolder.listFiles(File::isDirectory)) {
                for (File cardFile : cardFolder.listFiles()) {
                    try (BufferedReader br = new BufferedReader(new FileReader(cardFile.getAbsolutePath()))) {
                        JsonObject object = gson.fromJson(br, JsonObject.class);
                        String name = object.get("name").getAsString();
                        String type = object.get("type").getAsString();
                        String description = object.get("description").getAsString();
                        int manaCost =0;
                        String hero = Hero.NATURAL.getName();
                        String rarity = "";
                        if (object.get("manaCost") != null) {
                            manaCost = object.get("manaCost").getAsInt();
                            hero = object.get("hero").getAsString();
                            rarity = object.get("rarity").getAsString();
                        }
                        int attack;
                        int health;
                        if (object.get("attack") != null) {
                            attack = object.get("attack").getAsInt();
                            health = object.get("health").getAsInt();
                        } else {
                            attack = 0;
                            health = 0;
                        }
                        JsonElement ability = object.get("ability");
                        String abilityName = "";
                        int abilityValue = 0;
                        if (ability != null) {
                            abilityName = ability.getAsJsonObject().get("name").getAsString();
                            if (ability.getAsJsonObject().get("value") != null) {
                                abilityValue = ability.getAsJsonObject().get("value").getAsInt();
                            } else {
                                abilityValue = 0;
                            }
                        }
                        String heroIdQuery = defaultHeroIdSQL;
                        if (Objects.requireNonNull(Hero.getByName(hero)).getDefaultCards().contains(name)) {
                            heroIdQuery = String.format(defaultHeroIdSQL, hero);
                        }
//                        database.getWriter().insertData(index, name, manaCost, type, hero, rarity, attack, health, description, abilityName, abilityValue);
                        database.getWriter().insertData(insertDataSQL,
                                Arrays.asList(index, name, manaCost, type,
                                        database.getReader().getData(String.format(defaultHeroIdSQL, hero)),
                                        rarity, attack, health, description, abilityName, abilityValue,
                                        database.getReader().getData(heroIdQuery)));
                        index++;
                    }
                }
            }
        }
    }
}
