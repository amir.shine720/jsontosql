package database;

import java.sql.*;
import java.util.List;

public class MySqlWriter {
    private static final String createTableSQL = "create table cards(\r\n" + " id int(3) primary key, \r\n" +
            " name varchar(20),\r\n" +
            " manaCost int(3),\r\n" +
            " type varchar(20),\r\n" +
            " hero varchar(20),\r\n" +
            " rarity varchar(20),\r\n" +
            " attack int(3),\r\n" +
            " health int(3),\r\n" +
            " description TEXT,\r\n" +
            " abilityName varchar(20),\r\n" +
            " abilityValue int(3)\r\n );";
    private static final String insertDataSQL = "INSERT INTO cards" +
            " (id, name, manaCost, type, hero, rarity, attack, health, description, abilityName, abilityValue) VALUES " +
            " (?,?,?,?,?,?,?,?,?,?,?);";
    private static final String selectDataSQL = "select id,name,manaCost,type,hero,rarity,attack,health,description,abilityName,abilityValue from users where id = ?";
    private static final String deleteDataSQL = "delete from cards where id = ?;";
    private static final String dropTableSQL = "drop table cards";
    String tableName;

    public void createTable(String query) throws SQLException {
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/hearthstone?useSSL=false", "root", "root");
             Statement statement = connection.createStatement();) {

            statement.execute(query);
        }
    }

    public void insertData(String query, List<Object> data) throws SQLException {
        try (Connection connection = DriverManager
                .getConnection("jdbc:mysql://localhost:3306/hearthstone?useSSL=false", "root", "root");
             PreparedStatement preparedStatement = connection.prepareStatement(query)
        ) {
            int counter = 1;
            for (Object datum : data) {
                if (datum instanceof String) {
                    preparedStatement.setString(counter, String.valueOf(datum));
                } else if (datum instanceof Integer) {
                    preparedStatement.setInt(counter, (Integer) datum);
                } else if (datum instanceof Boolean) {
                    preparedStatement.setBoolean(counter, ((Boolean) datum));
                } else if (datum == null){
                    preparedStatement.setNull(counter, java.sql.Types.INTEGER);
                }
                counter++;
            }

            preparedStatement.executeUpdate();
        }
    }

    public void updateData() throws SQLException {
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://");
             PreparedStatement preparedStatement = connection.prepareStatement(selectDataSQL)
        ) {
            preparedStatement.setInt(1, 1);

            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.executeQuery();
        }
    }

    public void deleteData() throws SQLException {
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://");
             PreparedStatement preparedStatement = connection.prepareStatement(deleteDataSQL)
        ) {
            preparedStatement.setInt(1, 1);

            preparedStatement.executeUpdate();
        }
    }

}
