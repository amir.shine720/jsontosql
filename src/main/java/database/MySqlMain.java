package database;

public class MySqlMain {
    MySqlWriter writer;
    MySqlReader reader;

    public MySqlMain(){
        writer = new MySqlWriter();
        reader = new MySqlReader();
    }

    public MySqlReader getReader(){
        return reader;
    }
    public MySqlWriter getWriter(){
        return writer;
    }
}
