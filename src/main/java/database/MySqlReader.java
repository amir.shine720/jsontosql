package database;

import java.sql.*;

public class MySqlReader {

    private static final String ALL_DATA_QUERY = "select id,username,password";

    public Integer getData(String query) throws SQLException {
        try(Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/hearthstone?useSSL=false", "root", "root");
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query)){

            if (resultSet.next()){
                return resultSet.getInt("id");
            }
            return null;
        }
    }
}
